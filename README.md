# Description

Just a game of life repo.
Purposes:
* Simple canonical game of life js implementations.
* Simple rest/ws server.
* Provide an ability to create spec compatible rest/ws server on technology of your choise
## TODO

- engine
    - [x] Calc diff between two worlds
    - [x] Apply diff to state to create new state
    - [x] ProcessStep which returns diff
    - [x] Revert diff
    - [x] Serialize/deserialize from formatted text
- rest interface
    - [ ] describe API
    - [ ] adopt tests to api tests
    - [ ] cli to test API
    - [ ] benchmark test
    - [ ] client
- ws interface
    - [ ] describe api
    - [ ] adopt tests to api tests
    - [ ] cli to test API
    - [ ] benchmark test
    - [ ] client
- docker
    - [ ] docker guide 
- [ ] simple nodejs server
    - [ ] ws
    - [ ] http