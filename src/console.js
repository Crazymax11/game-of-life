import engine from './engine/index';

/*
    ...OO...
	..O..O..
	.O....O.
	O......O
	O......O
	.O....O.
	..O..O..
    ...OO...
    */

let world = engine.createWorld(
  [
    [0, 3],
    [0, 4],
    [1, 2],
    [1, 5],
    [2, 1],
    [2, 6],
    [3, 0],
    [3, 7],
    [4, 0],
    [4, 7],
    [5, 1],
    [5, 6],
    [6, 2],
    [6, 5],
    [7, 3],
    [7, 4]
  ],
  8,
  8
);
let counter = 0;
setInterval(() => {
  counter++;
  console.log(`STEP ${counter}`);
  console.log(JSON.stringify(world));
  const newWorld = engine.processStep(world);
  for (let i = 0; i < newWorld.height; i++) {
    let line = '';
    for (let j = 0; j < newWorld.width; j++) {
      const cell = engine.getCell(newWorld, i, j);
      if (cell.alive) {
        line += '# ';
      } else {
        line += 'o ';
      }
    }
    console.log(line);
    world = newWorld;
  }
}, 1000);
