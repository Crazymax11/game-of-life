// @flow

import type { Cell } from '../cell';
import {
  isAlive,
  setAlive,
  create as createCell,
  copy as copyCell,
  isEqual as isEqualCells
} from '../cell';

type CellCoordAxe = number;
type CellCoord = string;
type Cells = Map<CellCoord, Cell>;
type JSONCell = [CellCoordAxe, CellCoordAxe];
type size = number;
type World = {
  width: size,
  height: size,
  cells: Cells,
  toJSON: World => Object
};

export function processStep(w: World): World {
  const result = {
    width: w.width,
    height: w.height,
    cells: new Map(),
    toJSON
  };
  for (let i = 0; i < w.width; i++) {
    for (let j = 0; j < w.height; j++) {
      setCell(result, i, j, processCell(w, i, j));
    }
  }

  return result;
}

// TODO: Optimize process instead of wrap plain method
// Seems store diff will be faster (for CPU) then store new world and calc diff
export function processStepDiff(world: World): WorldDiff {
  return getDiff(world, processStep(world));
}

function processCell(w: World, x: number, y: number) {
  const cell = copyCell(getCell(w, x, y));
  const neighbors = countNeighbors(w, x, y);
  if (isAlive(cell) && neighbors > 1 && neighbors < 4) {
    setAlive(true, cell);
  } else if (!isAlive(cell) && neighbors === 3) {
    setAlive(true, cell);
  } else {
    setAlive(false, cell);
  }

  return cell;
}

function countNeighbors(w: World, x: number, y: number) {
  const neighborsCoords = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1]
  ];

  const count = neighborsCoords
    .map(([i, j]) => getCell(w, x + i, y + j))
    .filter(isAlive).length;

  return count;
}

export function create(cells: JSONCell[], width: size, height: size): World {
  const tcells = new Map();
  cells
    .map(([x, y]) => getKey(x, y))
    .forEach(key => tcells.set(key, createCell(true)));
  return {
    width,
    height,
    cells: tcells,
    toJSON
  };
}

export function getCell(world: World, x: CellCoordAxe, y: CellCoordAxe) {
  if (x >= world.width || y >= world.height) {
    return {
      alive: false
    };
  }
  const cell = world.cells.get(getKey(x, y));
  if (cell) return cell;
  return {
    alive: false
  };
}

function getKey(x: CellCoordAxe, y: CellCoordAxe): CellCoord {
  return `${x}-${y}`;
}

export function setCell(
  w: World,
  x: CellCoordAxe,
  y: CellCoordAxe,
  cell: Cell
): World {
  if (x >= w.width || y >= w.height) {
    return w;
  }

  if (isAlive(cell)) {
    w.cells.set(getKey(x, y), cell);
  } else {
    w.cells.delete(getKey(x, y));
  }

  return w;
}

function parseKey(key: CellCoord): JSONCell {
  const coords = key.split('-');
  if (coords.length === 2) {
    const c = coords.map(v => parseInt(v, 10));
    return [c[0], c[1]];
  }

  throw new Error();
}

function toJSON() {
  const world: World = this;
  const object = {
    width: world.width,
    height: world.height,
    cells: []
  };

  for (const cell of world.cells) {
    object.cells.push(parseKey(cell[0]));
  }
  return object;
}

export function isEqual(world1: World, world2: World): boolean {
  return (
    world1.width === world2.width &&
    world1.height === world2.height &&
    world1.cells.size === world2.cells.size &&
    isMapsEquals(world1.cells, world2.cells)
  );
}

export function isMapsEquals(cellMap1: Cells, cellMap2: Cells): boolean {
  for (const [key, cell] of cellMap1) {
    if (!cellMap2.has(key)) {
      return false;
    }
    // $FlowFixMe https://github.com/facebook/flow/issues/2876
    if (!isEqualCells(cellMap2.get(key), cell)) {
      return false;
    }
  }

  return true;
}

type WorldDiff = {
  add: JSONCell[],
  remove: JSONCell[]
};

export function getDiff(worldFrom: World, worldTo: World): WorldDiff {
  const added = [];
  const removed = [];
  for (const key of worldTo.cells.keys()) {
    if (!worldFrom.cells.has(key)) {
      added.push(key);
    }
  }

  for (const key of worldFrom.cells.keys()) {
    if (!worldTo.cells.has(key)) {
      removed.push(key);
    }
  }

  return {
    add: added.map(parseKey),
    remove: removed.map(parseKey)
  };
}

export function copy(world: World): World {
  const tcells = new Map();
  for (const [key, cell] of world.cells) {
    tcells.set(key, copyCell(cell));
  }
  return {
    width: world.width,
    height: world.height,
    cells: tcells,
    toJSON
  };
}

export function applyDiff(world: World, diff: WorldDiff): World {
  const newWorld = copy(world);

  diff.add.forEach(([x, y]) => {
    setCell(newWorld, x, y, createCell(true));
  });

  diff.remove.forEach(([x, y]) => {
    setCell(newWorld, x, y, createCell(false));
  });

  return newWorld;
}

export function revertDiff(diff: WorldDiff): WorldDiff {
  return {
    remove: diff.add,
    add: diff.remove
  };
}

export function isDiffEqual(diff: WorldDiff, diff2: WorldDiff): boolean {
  return (
    diff.add.length === diff2.add.length &&
    diff.remove.length === diff2.remove.length &&
    diff.add.every(d =>
      diff2.add.find(d2 => d2[0] === d[0] && d2[1] === d[1])
    ) &&
    diff.remove.every(d =>
      diff2.remove.find(d2 => d2[0] === d[0] && d2[1] === d[1])
    )
  );
}

type SerializedWorld = string;
export function serialize(world: World): SerializedWorld {
  let result = '';
  for (let y = 0; y < world.height; y++) {
    for (let x = 0; x < world.width; x++) {
      const cell = getCell(world, x, y);
      result += isAlive(cell) ? '#' : '-';
    }
    result += '\n';
  }

  return result;
}

export function deserialize(serialized: SerializedWorld): World {
  const rows = serialized.split('\n').slice(0, -1);
  const height = rows.length;
  const width = rows[0].length;
  const cells = [];
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      const cell = rows[y][x];
      if (cell === '#') {
        cells.push([x, y]);
      }
    }
  }

  return create(cells, width, height);
}
