import chai from 'chai';
import {
  create,
  getCell,
  applyDiff,
  processStep,
  processStepDiff,
  isEqual,
  getDiff,
  revertDiff,
  isDiffEqual,
  serialize,
  deserialize
} from './index';

chai.should();
const testData = require('./test.data.json');

const tests = {
  getCell({ title, input, expectedResult }) {
    it(title, () => {
      const w = create(
        input.world.cells,
        input.world.width,
        input.world.height
      );
      getCell(w, input.x, input.y).should.deep.equal(expectedResult);
    });
  },
  processStep({ title, input, expectedResult }) {
    it(title, () => {
      const w = create(
        input.world.cells,
        input.world.width,
        input.world.height
      );
      const newWorld = processStep(w);
      getCell(newWorld, 1, 1).should.deep.equal(expectedResult);
    });
  },
  ProcessStepFigure({ title, input, expectedResult }) {
    it(title, () => {
      let world = create(input.cells, input.width, input.height);
      expectedResult.forEach(expectedWorld => {
        world = processStep(world);
        const eworld = create(
          expectedWorld.cells,
          expectedWorld.width,
          expectedWorld.height
        );
        isEqual(world, eworld).should.be.true;
      });
    });
  },
  toJSON({ title, input, expectedResult }) {
    it(title, () => {
      const world = create(
        input.world.cells,
        input.world.width,
        input.world.height
      );
      const json = world.toJSON();
      JSON.parse(JSON.stringify(json)).should.be.deep.equal(expectedResult);
      JSON.stringify(world).should.be.equal(JSON.stringify(expectedResult));
    });
  },
  getDiff({ title, input, expectedResult }) {
    it(title, () => {
      const worldFrom = create(
        input.world1.cells,
        input.world1.width,
        input.world1.height
      );
      const worldTo = create(
        input.world2.cells,
        input.world2.width,
        input.world2.height
      );

      getDiff(worldFrom, worldTo).should.be.deep.equal(expectedResult);
    });
  },
  applyDiff({ title, input, expectedResult }) {
    it(title, () => {
      const world = create(
        input.world.cells,
        input.world.width,
        input.world.height
      );
      isEqual(
        applyDiff(world, input.diff),
        create(
          expectedResult.cells,
          expectedResult.width,
          expectedResult.height
        )
      ).should.be.true;
    });
  },
  applyDiffShouldCopy({ title, input }) {
    it(title, () => {
      const world = create(
        input.world.cells,
        input.world.width,
        input.world.height
      );
      applyDiff(world, input.diff).should.not.be.equal(world);
    });
  },
  revertDiff({ title, input, expectedResult }) {
    it(title, () => {
      revertDiff(input).should.be.deep.equal(expectedResult);
    });
  },
  processStepDiff({ title, input, expectedResult }) {
    it(title, () => {
      isDiffEqual(
        processStepDiff(
          create(input.world.cells, input.world.width, input.world.height)
        ),
        expectedResult
      ).should.be.true;
    });
  },
  serialize({ title, input, expectedResult }) {
    it(title, () => {
      serialize(create(input.cells, input.width, input.height)).should.be.equal(
        expectedResult
      );
    });
  },
  deserialize({ title, input, expectedResult }) {
    it(title, () => {
      isEqual(
        deserialize(input),
        create(
          expectedResult.cells,
          expectedResult.width,
          expectedResult.height
        )
      ).should.be.true;
    });
  }
};

describe('engine', () => {
  Object.keys(testData).forEach(key => {
    describe(`#${key}`, () => {
      testData[key].forEach(tests[key]);
    });
  });
});
