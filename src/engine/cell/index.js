/* @flow */

export type AliveProp = boolean;
export type Cell = {
  alive: AliveProp
};

export function isAlive(c: Cell): AliveProp {
  return c.alive;
}

export function setAlive(alive: AliveProp, cell: Cell): Cell {
  cell.alive = alive;
  return cell;
}

export function create(alive: AliveProp): Cell {
  return {
    alive
  };
}

export function copy(cell: Cell): Cell {
  return Object.assign({}, cell);
}

export function isEqual(cell1: Cell, cell2: Cell): boolean {
  return cell1.alive === cell2.alive;
}

export default {};
