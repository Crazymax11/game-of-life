import chai from 'chai';

import { create, copy, isEqual, setAlive, isAlive } from './index';

chai.should();

const aliveCell = {
  alive: true
};

const deadCell = {
  alive: false
};

describe('Cell', () => {
  describe('#create', () => {
    it('should create cell', () => {
      create(true).should.be.deep.equal(aliveCell);
    });
  });
  describe('#copy', () => {
    it('should copy alive cell', () => {
      const aliveCopy = copy(aliveCell);
      aliveCopy.should.be.deep.equal(aliveCell);
      aliveCopy.should.not.be.equal(aliveCell);
    });
    it('should copy dead cell', () => {
      const deadCopy = copy(deadCell);
      deadCopy.should.be.deep.equal(deadCell);
      deadCopy.should.not.be.equal(deadCell);
    });
  });
  describe('#isEqual', () => {
    it('should return true for equal cells', () => {
      const copyCell = copy(aliveCell);
      isEqual(copyCell, aliveCell).should.be.true;
    });
    it('should return false for not equal cells', () => {
      const copyCell = copy(aliveCell);
      isEqual(copyCell, deadCell).should.be.false;
    });
  });
  describe('#isAlive', () => {
    it('should return true for alive cell', () => {
      isAlive(aliveCell).should.be.true;
    });
    it('should return false for dead cell', () => {
      isAlive(deadCell).should.be.false;
    });
  });
  describe('#setAlive', () => {
    it('should set alive to alive cell', () => {
      isAlive(setAlive(true, copy(aliveCell))).should.be.true;
    });
    it('should unset alive to alive cell', () => {
      isAlive(setAlive(false, copy(aliveCell))).should.be.false;
    });
    it('should set alive to dead cell', () => {
      isAlive(setAlive(true, copy(deadCell))).should.be.true;
    });
    it('should set alive to alive cell', () => {
      isAlive(setAlive(false, copy(deadCell))).should.be.false;
    });
  });
});
