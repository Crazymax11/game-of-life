const path = require('path');

module.exports = {
  entry: {
    console: './src/console.js'
  },
  target: 'node',
  output: {
    path: path.resolve('dist'),
    filename: 'console.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader'
      }
    ]
  }
};
