/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__engine_index__ = __webpack_require__(1);


/*
    ...OO...
	..O..O..
	.O....O.
	O......O
	O......O
	.O....O.
	..O..O..
    ...OO...
    */

var world = __WEBPACK_IMPORTED_MODULE_0__engine_index__["a" /* default */].createWorld([[0, 3], [0, 4], [1, 2], [1, 5], [2, 1], [2, 6], [3, 0], [3, 7], [4, 0], [4, 7], [5, 1], [5, 6], [6, 2], [6, 5], [7, 3], [7, 4]], 8, 8);
var counter = 0;
setInterval(function () {
  counter++;
  console.log('STEP ' + counter);
  console.log(JSON.stringify(world));
  var newWorld = __WEBPACK_IMPORTED_MODULE_0__engine_index__["a" /* default */].processStep(world);
  for (var i = 0; i < newWorld.height; i++) {
    var line = '';
    for (var j = 0; j < newWorld.width; j++) {
      var cell = __WEBPACK_IMPORTED_MODULE_0__engine_index__["a" /* default */].getCell(newWorld, i, j);
      if (cell.alive) {
        line += '# ';
      } else {
        line += 'o ';
      }
    }
    console.log(line);
    world = newWorld;
  }
}, 1000);

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cell__ = __webpack_require__(2);
var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();



function processStep(w) {
  var result = {
    width: w.width,
    height: w.height,
    cells: new Map(),
    toJSON: toJSON
  };
  for (var i = 0; i < w.width; i++) {
    for (var j = 0; j < w.height; j++) {
      setCell(result, i, j, processCell(w, i, j));
    }
  }

  return result;
}

function processCell(w, x, y) {
  var cell = Object(__WEBPACK_IMPORTED_MODULE_0__cell__["a" /* copyCell */])(getCell(w, x, y));
  var neighbors = countNeighbors(w, x, y);
  if (Object(__WEBPACK_IMPORTED_MODULE_0__cell__["c" /* isAlive */])(cell) && neighbors > 1 && neighbors < 4) {
    Object(__WEBPACK_IMPORTED_MODULE_0__cell__["d" /* setAlive */])(true, cell);
  } else if (!Object(__WEBPACK_IMPORTED_MODULE_0__cell__["c" /* isAlive */])(cell) && neighbors === 3) {
    Object(__WEBPACK_IMPORTED_MODULE_0__cell__["d" /* setAlive */])(true, cell);
  } else {
    Object(__WEBPACK_IMPORTED_MODULE_0__cell__["d" /* setAlive */])(false, cell);
  }

  return cell;
}

function countNeighbors(w, x, y) {
  var neighborsCoords = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];

  var count = neighborsCoords.map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        i = _ref2[0],
        j = _ref2[1];

    return getCell(w, x + i, y + j);
  }).filter(__WEBPACK_IMPORTED_MODULE_0__cell__["c" /* isAlive */]).length;

  return count;
}

function createWorld(cells, width, height) {
  var tcells = new Map();
  cells.map(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
        x = _ref4[0],
        y = _ref4[1];

    return getKey(x, y);
  }).forEach(function (key) {
    return tcells.set(key, Object(__WEBPACK_IMPORTED_MODULE_0__cell__["b" /* createCell */])(true));
  });
  return {
    width: width,
    height: height,
    cells: tcells,
    toJSON: toJSON
  };
}

function getCell(world, x, y) {
  if (x >= world.width || y >= world.height) {
    return {
      alive: false
    };
  }
  var cell = world.cells.get(getKey(x, y));
  if (cell) return cell;
  return {
    alive: false
  };
}

function getKey(x, y) {
  return x + '-' + y;
}

function setCell(w, x, y, cell) {
  if (x >= w.width || y >= w.height) {
    return w;
  }

  if (Object(__WEBPACK_IMPORTED_MODULE_0__cell__["c" /* isAlive */])(cell)) {
    w.cells.set(getKey(x, y), cell);
  } else {
    w.cells.delete(getKey(x, y));
  }

  return w;
}

function parseKey(key) {
  var coords = key.split('-');
  if (coords.length === 2) {
    var c = coords.map(function (v) {
      return parseInt(v, 10);
    });
    return [c[0], c[1]];
  }

  throw new Error();
}

function toJSON() {
  var world = this;
  var object = {
    width: world.width,
    height: world.height,
    cells: []
  };

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = world.cells[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var cell = _step.value;

      object.cells.push(parseKey(cell[0]));
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return object;
}

/* harmony default export */ __webpack_exports__["a"] = ({
  createWorld: createWorld,
  processStep: processStep,
  getCell: getCell,
  getKey: getKey
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["c"] = isAlive;
/* harmony export (immutable) */ __webpack_exports__["d"] = setAlive;
/* harmony export (immutable) */ __webpack_exports__["b"] = createCell;
/* harmony export (immutable) */ __webpack_exports__["a"] = copyCell;
function isAlive(c) {
  return c.alive;
}

function setAlive(alive, cell) {
  cell.alive = alive;
  return cell;
}

function createCell(alive) {
  return {
    alive: alive
  };
}

function copyCell(cell) {
  return Object.assign({}, cell);
}

/***/ })
/******/ ]);